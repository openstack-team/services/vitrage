vitrage (13.0.0-5) unstable; urgency=medium

  * Add Multi-Arch: foreign for the -doc package.

 -- Thomas Goirand <zigo@debian.org>  Fri, 27 Dec 2024 15:26:00 +0100

vitrage (13.0.0-4) unstable; urgency=medium

  * Switch -doc to bootstrap 5 (Closes: #1088536).

 -- Thomas Goirand <zigo@debian.org>  Sat, 21 Dec 2024 10:39:24 +0100

vitrage (13.0.0-3) unstable; urgency=medium

  * Removed -O--buildsystem=python_distutils from d/rules.

 -- Thomas Goirand <zigo@debian.org>  Fri, 20 Dec 2024 10:45:20 +0100

vitrage (13.0.0-2) unstable; urgency=medium

  * Switch to pybuild (Closes: #1090707).

 -- Thomas Goirand <zigo@debian.org>  Wed, 18 Dec 2024 14:00:37 +0100

vitrage (13.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 02 Oct 2024 16:27:37 +0200

vitrage (13.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 20 Sep 2024 17:31:25 +0200

vitrage (13.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * autopkgtest: blacklist test_create_update_entity_vertex.

 -- Thomas Goirand <zigo@debian.org>  Tue, 17 Sep 2024 10:14:19 +0200

vitrage (12.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 08 Apr 2024 10:02:28 +0200

vitrage (12.0.0~rc1-3) unstable; urgency=medium

  * Fix blacklist set in 12.0.0~rc1-2.
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Tue, 19 Mar 2024 08:50:20 +0100

vitrage (12.0.0~rc1-2) experimental; urgency=medium

  * Blacklist unit tests failing on Bookworm backport (but fine on Unstable):
    - EquivalentScenarioTest.test_expansion
    - TestEquivalenceRepository.test_duplicate_entities_in_equivalence
    - RegExTemplateTest.test_basic_regex

 -- Thomas Goirand <zigo@debian.org>  Mon, 18 Mar 2024 09:45:29 +0100

vitrage (12.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Sun, 17 Mar 2024 14:16:06 +0100

vitrage (11.0.0-2) unstable; urgency=medium

  * Use python3-pysnmp-lextudio instead of python3-pysnmp4. (Closes: #1058159).

 -- Thomas Goirand <zigo@debian.org>  Tue, 12 Dec 2023 22:32:42 +0100

vitrage (11.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 05 Oct 2023 11:25:36 +0200

vitrage (11.0.0~rc1-2) experimental; urgency=medium

  * Add python3-webtest as build-depends.

 -- Thomas Goirand <zigo@debian.org>  Sun, 17 Sep 2023 10:55:10 +0200

vitrage (11.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Cleans better.

 -- Thomas Goirand <zigo@debian.org>  Sun, 17 Sep 2023 10:10:36 +0200

vitrage (10.0.0-2) unstable; urgency=medium

  * Uploading to unstable.
  * Removed obsolete lsb-base depends.

 -- Thomas Goirand <zigo@debian.org>  Mon, 19 Jun 2023 14:53:08 +0200

vitrage (10.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 22 Mar 2023 14:49:44 +0100

vitrage (10.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Removed versions from (build-)depends when satisfied in Bookworm.
  * Add python3-webtest as build-depends.
  * Removed Fix-compat-with-oslo.db-12.1.0.patch applied upstream.

 -- Thomas Goirand <zigo@debian.org>  Mon, 06 Mar 2023 09:10:00 +0100

vitrage (9.0.0-3) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sat, 24 Sep 2022 17:10:08 +0200

vitrage (9.0.0-2) experimental; urgency=medium

  * Add Fix-compat-with-oslo.db-12.1.0.patch.

 -- Thomas Goirand <zigo@debian.org>  Wed, 21 Sep 2022 09:43:13 +0200

vitrage (9.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 15 Sep 2022 16:02:40 +0200

vitrage (8.0.1-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 28 Mar 2022 09:25:57 +0200

vitrage (8.0.0-1) experimental; urgency=medium

  * New upstream release.
  * Add autopkgtest.

 -- Thomas Goirand <zigo@debian.org>  Sun, 27 Feb 2022 11:41:46 +0100

vitrage (7.5.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 30 Sep 2021 15:47:09 +0200

vitrage (7.5.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 21 Sep 2021 09:30:30 +0200

vitrage (7.4.0-2) unstable; urgency=medium

  * Upload to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 16 Aug 2021 14:38:08 +0200

vitrage (7.4.0-1) experimental; urgency=medium

  * New upstream release.
  * Removed (build-)depends versions when satisfied in Bullseye.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Sun, 28 Mar 2021 12:53:53 +0200

vitrage (7.3.0-2) unstable; urgency=medium

  * Uploading to unstable.
  * Fixed debian/watch.
  * Add a debian/salsa-ci.yml.

 -- Thomas Goirand <zigo@debian.org>  Sat, 17 Oct 2020 21:15:03 +0200

vitrage (7.3.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Use a policy.d and yaml policy file.
  * d/rules: Removed --with systemd.
  * Switch to debhelper-compat 11.
  * Standards-Version: 4.5.0.

 -- Thomas Goirand <zigo@debian.org>  Thu, 24 Sep 2020 21:23:01 +0200

vitrage (7.2.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Fri, 11 Sep 2020 09:58:30 +0200

vitrage (7.1.0-3) unstable; urgency=medium

  * Reupload source-only

 -- Michal Arbet <michal.arbet@ultimum.io>  Fri, 12 Jun 2020 13:01:55 +0200

vitrage (7.1.0-2) unstable; urgency=medium

  * Upload to unstable from experimental

 -- Michal Arbet <michal.arbet@ultimum.io>  Fri, 12 Jun 2020 10:51:18 +0200

vitrage (7.1.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 30 Apr 2020 22:24:28 +0200

vitrage (7.0.0-2) experimental; urgency=medium

  * Add missing override_dh_python3.

 -- Thomas Goirand <zigo@debian.org>  Thu, 09 Apr 2020 09:46:44 +0200

vitrage (7.0.0-1) experimental; urgency=medium

  * d/patches:
    - Remove fix-tests.patch
    - fix-networkx-compatibility.patch
  * d/control: Fix requirements

 -- Michal Arbet <michal.arbet@ultimum.io>  Wed, 08 Apr 2020 21:02:24 +0200

vitrage (5.0.1-1) unstable; urgency=medium

  * New upstream version.
  * d/patches: Add fix-networkx-compatibility.patch (Closes: #952263).
  * d/patches/fix-tests.patch
  * d/control: Add python3-alembic dependency.

 -- Michal Arbet <michal.arbet@ultimum.io>  Mon, 24 Feb 2020 12:00:55 +0100

vitrage (5.0.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Thomas Goirand ]
  * Uploading to unstable.
  * Add fix-requirements.txt-to-allow-networkx-2.2.patch.

 -- Thomas Goirand <zigo@debian.org>  Tue, 22 Oct 2019 22:51:33 +0200

vitrage (5.0.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.0.

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Refreshed reproducible-build.patch.
  * Black listed (no time to investigate):
    - test_create_update_entity_vertex()
    - test_basic_regex_with_no_match()

 -- Thomas Goirand <zigo@debian.org>  Wed, 02 Oct 2019 09:31:29 +0200

vitrage (4.3.1-3) unstable; urgency=medium

  * Uploading to unstable.
  * Blacklist TemplateLoaderTest.test_basic_template_with_include().

 -- Michal Arbet <michal.arbet@ultimum.io>  Wed, 17 Jul 2019 20:10:11 +0200

vitrage (4.3.1-2) experimental; urgency=medium

  * d/control: Bump openstack-pkg-tools to version 99

 -- Michal Arbet <michal.arbet@ultimum.io>  Thu, 02 May 2019 15:08:43 +0200

vitrage (4.3.1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 11 Apr 2019 11:13:09 +0200

vitrage (4.3.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Running wrap-and-sort -bast

  [ Thomas Goirand ]
  * New upstream release.
  * Standards-Version is now 4.3.0.
  * Removed package versions when satisfied in Buster.
  * Fixed (build-)depends for this release.
  * Blacklist 2 more unit tests:
    - test_template_validator_v3.TemplateValidatorV3Test.test_conditions
    - test_template_validator_v3.TemplateValidatorV3Test.test_mark_down

 -- Thomas Goirand <zigo@debian.org>  Sat, 30 Mar 2019 12:17:22 +0100

vitrage (3.2.0-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable:
    - Fixes FTBFS (Closes: #906417).

 -- Thomas Goirand <zigo@debian.org>  Mon, 03 Sep 2018 16:27:13 +0200

vitrage (3.1.0-1.1) experimental; urgency=medium

  [ Michal Arbet ]
  * New upstream release
  * Add reproducible-build.patch (Closes: #899162).
  * Fixed (build-)depends for this release.

  [ Ondřej Nový ]
  * d/control: Use team+openstack@tracker.debian.org as maintainer

  [ Thomas Goirand ]
  * Only run unit tests, not functional.
  * Blacklist failing tests:
    - evaluator.test_scenario_repository.ScenarioRepositoryTest()
    - test_scenario_repository.RegExTemplateTest.test_regex_with_exact_match()

 -- Thomas Goirand <zigo@debian.org>  Fri, 31 Aug 2018 12:04:41 +0200

vitrage (2.2.0-1) unstable; urgency=medium

  [ Michal Arbet ]
  * Initial release (Closes: #893095).

 -- Thomas Goirand <zigo@debian.org>  Mon, 26 Mar 2018 15:04:55 +0000
